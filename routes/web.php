<?php

/** @var \Laravel\Lumen\Routing\Router $router */

$router->group(['prefix' => 'api'], function () use ($router) {
    $router->get('/getCategories', 'EnumController@getCategories');
    $router->get('/getStates', 'EnumController@getStates');
    $router->get('/getMessageOptions', 'EnumController@getMessageOptions');
    $router->get('/getServiceTypes', 'EnumController@getServiceTypes');
    $router->get('/getBrands', 'EnumController@getBrands');
    $router->get('/getLanguages', 'EnumController@getLanguages');
    $router->get('/getAllEnums', 'EnumController@getAllEnums');
});
