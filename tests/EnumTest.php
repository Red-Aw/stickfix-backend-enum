<?php

use App\Http\Controllers\EnumController;

class EnumTest extends TestCase
{
    public function testGetCategoriesUnauthorized()
    {
        $this->json('GET', 'api/getCategories', [], [ 'Accept' => 'application/json', 'Authorization' => '' ])
            ->seeStatusCode(401);
    }

    public function testGetCategories()
    {
        $this->json('GET', 'api/getCategories', [], [ 'Accept' => 'application/json', 'Authorization' => env('ALLOWED_SECRET') ])
            ->seeStatusCode(200)
            ->seeJsonContains([
                'success' => true,
                'data' => [
                    'CATEGORIES' => EnumController::CATEGORIES
                ]
            ]);
    }

    public function testGetStatesUnauthorized()
    {
        $this->json('GET', 'api/getStates', [], [ 'Accept' => 'application/json', 'Authorization' => '' ])
            ->seeStatusCode(401);
    }

    public function testGetStates()
    {
        $this->json('GET', 'api/getStates', [], [ 'Accept' => 'application/json', 'Authorization' => env('ALLOWED_SECRET') ])
            ->seeStatusCode(200)
            ->seeJsonContains([
                'success' => true,
                'data' => [
                    'STATES' => EnumController::STATES
                ]
            ]);
    }

    public function testGetMessageOptionsUnauthorized()
    {
        $this->json('GET', 'api/getMessageOptions', [], [ 'Accept' => 'application/json', 'Authorization' => '' ])
            ->seeStatusCode(401);
    }

    public function testGetMessageOptions()
    {
        $this->json('GET', 'api/getMessageOptions', [], [ 'Accept' => 'application/json', 'Authorization' => env('ALLOWED_SECRET') ])
            ->seeStatusCode(200)
            ->seeJsonContains([
                'success' => true,
                'data' => [
                    'MESSAGE_OPTIONS' => EnumController::MESSAGE_OPTIONS
                ]
            ]);
    }

    public function testGetServiceTypesUnauthorized()
    {
        $this->json('GET', 'api/getServiceTypes', [], [ 'Accept' => 'application/json', 'Authorization' => '' ])
            ->seeStatusCode(401);
    }

    public function testGetServiceTypes()
    {
        $this->json('GET', 'api/getServiceTypes', [], [ 'Accept' => 'application/json', 'Authorization' => env('ALLOWED_SECRET') ])
            ->seeStatusCode(200)
            ->seeJsonContains([
                'success' => true,
                'data' => [
                    'SERVICE_TYPES' => EnumController::SERVICE_TYPES
                ]
            ]);
    }

    public function testGetBrandsUnauthorized()
    {
        $this->json('GET', 'api/getBrands', [], [ 'Accept' => 'application/json', 'Authorization' => '' ])
            ->seeStatusCode(401);
    }

    public function testGetBrands()
    {
        $this->json('GET', 'api/getBrands', [], [ 'Accept' => 'application/json', 'Authorization' => env('ALLOWED_SECRET') ])
            ->seeStatusCode(200)
            ->seeJsonContains([
                'success' => true,
                'data' => [
                    'BRANDS' => EnumController::BRANDS
                ]
            ]);
    }

    public function testGetLanguagesUnauthorized()
    {
        $this->json('GET', 'api/getLanguages', [], [ 'Accept' => 'application/json', 'Authorization' => '' ])
            ->seeStatusCode(401);
    }

    public function testGetLanguages()
    {
        $this->json('GET', 'api/getLanguages', [], [ 'Accept' => 'application/json', 'Authorization' => env('ALLOWED_SECRET') ])
            ->seeStatusCode(200)
            ->seeJsonContains([
                'success' => true,
                'data' => [
                    'LANGUAGES' => EnumController::LANGUAGES
                ]
            ]);
    }

    public function testGetAllEnumsUnauthorized()
    {
        $this->json('GET', 'api/getAllEnums', [], [ 'Accept' => 'application/json', 'Authorization' => '' ])
            ->seeStatusCode(401);
    }

    public function testGetAllEnums()
    {
        $enumObj = new EnumController;

        $this->json('GET', 'api/getAllEnums', [], [ 'Accept' => 'application/json', 'Authorization' => env('ALLOWED_SECRET') ])
            ->seeStatusCode(200)
            ->seeJsonContains([
                'success' => true,
                'data' => [
                    'CATEGORIES' => EnumController::CATEGORIES,
                    'STATES' => EnumController::STATES,
                    'MESSAGE_OPTIONS' => EnumController::MESSAGE_OPTIONS,
                    'SERVICE_TYPES' => EnumController::SERVICE_TYPES,
                    'BRANDS' => EnumController::BRANDS,
                    'SIZES' => $enumObj->getSizes(),
                    'LANGUAGES' => EnumController::LANGUAGES,
                ]
            ]);
    }
}
