<?php

namespace App\Http\Controllers;

class EnumController extends Controller
{
    const CATEGORIES = array(
        'SKATES',
        'STICKS',
        'HELMET',
        'GLOVES',
        'PANTS',
        'SHOULDER_PADS',
        'SHIN_GUARDS',
        'ELBOW_PADS',
        'BAGS',
        'WEAR_AND_ACCESSORIES',
        'KITS',
        'KIDS_EQUIPMENT',
        'GOALIES_EQUIPMENT',
        'FIGURE_SKAITING',
        'INLINE',
        'OTHER',
    );

    const STATES = array(
        'NEW',
        'USED',
        'SLIGHTLY_USED',
    );

    const MESSAGE_OPTIONS = array(
        'APPLY_FOR_REPAIR',
        'APPLY_FOR_ITEM',
        'QUESTION',
        'OTHER',
    );

    const SERVICE_TYPES = array(
        'OFFER',
        'BUY',
    );

    const BRANDS = array(
        'NO',
        'BAUER',
        'CCM',
        'EASTON',
        'JOFA',
        'MISSION',
        'NIKE',
        'RBK',
        'TACKLA',
        'TRUE',
        'VAUGHN',
        'WARRIOR',
        'OTHER',
    );

    // Pants, Shoulder pads, Elbow pads sizes: YTH, JR, SR combined with S, M, L, XL
    const PANTS_SHOULDER_PADS_AND_ELBOW_PADS_SIZES = array(
        'SR_S',
        'SR_M',
        'SR_L',
        'SR_XL',
        'JR_S',
        'JR_M',
        'JR_L',
        'JR_XL',
        'YTH_S',
        'YTH_M',
        'YTH_L',
        'YTH_XL',
    );

    // Skates sizes:
    // Width size: Fit1, Fit2, Fit3, C, D, E, EE, Regular, Wide ...
    const SKATES_WIDTH_SIZES = array(
        'C', 'D', 'E', 'EE', 'R', 'Fit 1', 'Fit 2', 'Fit 3', 'Tapered', 'Regular', 'Wide'
    );

    // Lenght size: 1 - 15 and EUR: 33,5 - 51,0
    const SKATES_LENGTH_SIZES = array(
        '1.0' => [
            'US' => '2.0',
            'EUR' => '33.5',
            'UK' => '1.5',
        ],
        '1.5' => [
            'US' => '2.5',
            'EUR' => '34',
            'UK' => '2.0',
        ],
        '2.0' => [
            'US' => '3.0',
            'EUR' => '35',
            'UK' => '2.5',
        ],
        '2.5' => [
            'US' => '3.5',
            'EUR' => '35.5',
            'UK' => '3.0',
        ],
        '3.0' => [
            'US' => '4.0',
            'EUR' => '36',
            'UK' => '3.5',
        ],
        '3.5' => [
            'US' => '4.5',
            'EUR' => '36.5',
            'UK' => '4.0',
        ],
        '4.0' => [
            'US' => '5.0',
            'EUR' => '37.5',
            'UK' => '4.5',
        ],
        '4.5' => [
            'US' => '5.5',
            'EUR' => '38',
            'UK' => '5.0',
        ],
        '5.0' => [
            'US' => '6.0',
            'EUR' => '38.5',
            'UK' => '5.5',
        ],
        '5.5' => [
            'US' => '6.5',
            'EUR' => '39',
            'UK' => '6.0',
        ],
        '6.0' => [
            'US' => '7.5',
            'EUR' => '40.5',
            'UK' => '6.5',
        ],
        '6.5' => [
            'US' => '8.0',
            'EUR' => '41.0',
            'UK' => '7.0',
        ],
        '7.0' => [
            'US' => '8.5',
            'EUR' => '42.0',
            'UK' => '7.5',
        ],
        '7.5' => [
            'US' => '9.0',
            'EUR' => '42.5',
            'UK' => '8.0',
        ],
        '8.0' => [
            'US' => '9.5',
            'EUR' => '43.0',
            'UK' => '8.5',
        ],
        '8.5' => [
            'US' => '10.0',
            'EUR' => '44.0',
            'UK' => '9.0',
        ],
        '9.0' => [
            'US' => '10.5',
            'EUR' => '44.5',
            'UK' => '9.5',
        ],
        '9.5' => [
            'US' => '11.0',
            'EUR' => '45.0',
            'UK' => '10.0',
        ],
        '10.0' => [
            'US' => '11.5',
            'EUR' => '45.5',
            'UK' => '10.5',
        ],
        '10.5' => [
            'US' => '12.0',
            'EUR' => '46.0',
            'UK' => '11.0',
        ],
        '11.0' => [
            'US' => '12.5',
            'EUR' => '47.0',
            'UK' => '11.5',
        ],
        '11.5' => [
            'US' => '13.0',
            'EUR' => '47.5',
            'UK' => '12.0',
        ],
        '12.0' => [
            'US' => '13.5',
            'EUR' => '48.0',
            'UK' => '12.5',
        ],
        '12.5' => [
            'US' => '14.0',
            'EUR' => '48.5',
            'UK' => '13.0',
        ],
        '13.0' => [
            'US' => '14',
            'EUR' => '49',
            'UK' => '13.5',
        ],
        '14.0' => [
            'US' => '15',
            'EUR' => '50',
            'UK' => '14.5',
        ],
        '15.0' => [
            'US' => '16',
            'EUR' => '51',
            'UK' => '15.5',
        ],
    );

    // Sticks size: YTH, JR, INT, SR
    // Flex: 10 - 140
    const STICK_SIZES = array(
        'SR', 'INT', 'JR', 'YTH'
    );

    // Blade curve: P28, P29, P92 ...
    const STICK_BLADE_CURVE = array(
        'P92', 'PM9', 'P88', 'P91A', 'P106', 'P02', 'P14', 'P08',
        'P15 [102-2]', 'P19 [164-1]', 'P40 [182-1]', 'P45 [145-0]', 'P46 [197-2]'
    );

    // Blade curve: P28, P29, P92 ...
    const STICK_BLADE_SIDE = array(
        'LEFT', 'RIGHT', 'NONE',
    );

    // Helmet sizes: XS, S, M, L, XL
    const HELMET_SIZES = array(
        'XS', 'S', 'M', 'L', 'XL'
    );

    // Bags: SR, JR
    const BAG_SIZES = array(
        'SR', 'JR'
    );

    // Kits: YTH, M, L, XL
    const KIT_SIZES = array(
        'XL', 'L', 'M', 'YTH'
    );

    const LANGUAGES = array(
        'lv', 'en', 'ru',
    );

    public function getCategories()
    {
        return $this->response(true, '', ['CATEGORIES' => self::CATEGORIES]);
    }

    public function getStates()
    {
        return $this->response(true, '', ['STATES' => self::STATES]);
    }

    public function getMessageOptions()
    {
        return $this->response(true, '', ['MESSAGE_OPTIONS' => self::MESSAGE_OPTIONS]);
    }

    public function getServiceTypes()
    {
        return $this->response(true, '', ['SERVICE_TYPES' => self::SERVICE_TYPES]);
    }

    public function getBrands()
    {
        return $this->response(true, '', ['BRANDS' => self::BRANDS]);
    }

    public function getLanguages()
    {
        return $this->response(true, '', ['LANGUAGES' => self::LANGUAGES]);
    }

    public function getSizes()
    {
        return [
            'SHIN_GUARD' => $this->getInterval(8, 17), // Shin guards: 8 - 17
            'PANTS_SHOULDER_ELBOW_PAD_SIZES' => self::PANTS_SHOULDER_PADS_AND_ELBOW_PADS_SIZES,
            'SKATES' => array(
                'WIDTH' => self::SKATES_WIDTH_SIZES,
                'LENGTH' => self::SKATES_LENGTH_SIZES,
            ),
            'STICK' => array(
                'SIZE' => self::STICK_SIZES,
                'BLADE_CURVE' => self::STICK_BLADE_CURVE,
                'BLADE_SIDE' => self::STICK_BLADE_SIDE,
                'FLEX' => $this->getInterval(10, 140), // Flex: 10 - 140
            ),
            'HELMET' => self::HELMET_SIZES,
            'GLOVES' => $this->getInterval(8, 15), // Gloves: 8 - 15
            'BAG' => self::BAG_SIZES,
            'KIT' => self::KIT_SIZES,
        ];
    }

    public function getAllEnums()
    {
        return $this->response(true, '', [
            'CATEGORIES' => self::CATEGORIES,
            'STATES' => self::STATES,
            'MESSAGE_OPTIONS' => self::MESSAGE_OPTIONS,
            'SERVICE_TYPES' => self::SERVICE_TYPES,
            'BRANDS' => self::BRANDS,
            'SIZES' => $this->getSizes(),
            'LANGUAGES' => self::LANGUAGES,
        ]);
    }

    private function getInterval($min, $max)
    {
        $flex = [];
        for ($i = $min; $i <= $max; $i++) {
            $flex[] = $i;
        }
        return $flex;
    }
}
