<?php

namespace App\Traits;

use Illuminate\Http\Response;

use function response;

trait ApiResponse
{
    public function response($success, $message = null, $data = [], $code = Response::HTTP_OK, $errors = [])
    {
        return response()->json(
            ['success' => $success, 'message' => $message, 'data' => $data, 'code' => $code, 'errors' => $errors],
            $code
        );
    }
}
